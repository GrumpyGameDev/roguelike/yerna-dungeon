local imageregion = {}

function imageregion.new(image,srcRect)
  local instance = {}
  
  local values ={
  }
  
  values.image=image
  values.quad = love.graphics.newQuad(srcRect:getX(),srcRect:getY(),srcRect:getWidth(),srcRect:getHeight(),image:getDimensions())
  
  function instance:draw(x,y)
    love.graphics.draw(values.image,values.quad,x,y)
  end
  
  return instance
end

return imageregion