local control = require "common.controls.control"
local rectangle = {}

function rectangle.new(parent)
  local instance = control.new(parent)
  function instance:onDraw()
    love.graphics.rectangle("fill",self:getX()-self:getWidth()/2,self:getY()-self:getHeight()/2,self:getWidth(),self:getHeight())
  end
  return instance
end


return rectangle