local animation = {}
local json = require "lunajson"
local easingLib = require "lib.easing"

local mt = {__metatable="animation"}

function animation.new(easing,duration,changes,amplitude,period,autoRepeat,deleteAfter)
  assert(type(easing)=="string","animation.new - easing must be a string")
  assert(type(duration)=="number","animation.new - duration must be a number")
  assert(type(changes)=="table","animation.new - changes must be a changes")
  assert(type(amplitude)=="nil" or type(amplitude)=="number","animation.new - amplitude must be nil or a number")
  assert(type(period)=="nil" or type(period)=="number","animation.new - period must be nil or a number")
  assert(type(autoRepeat)=="nil" or type(autoRepeat)=="boolean","animation.new - autoRepeat must be nil or a boolean")
  assert(type(deleteAfter)=="nil" or type(deleteAfter)=="boolean","animation.new - deleteAfter must be nil or a boolean")
  
  local instance = {}
  setmetatable(instance,mt)
  
  local values = {
    easing = easing,
    time = 0,
    duration=duration,
    amplitude=amplitude,
    period=period,
    changes = json.decode(json.encode(changes)),
    autoRepeat = autoRepeat or false,
    deleteAfter = deleteAfter or false
  }
  
  function instance:update(dt)
    values.time = values.time + dt
    return values.time - values.duration
  end
  
  function instance:getValues()
    local result = {}
    if values.time >= values.duration then
      for k,v in pairs(values.changes) do
        result[k]=v.new
      end
    else
      for k,v in pairs(values.changes) do
        result[k]=easingLib[values.easing](values.time,v.old,v.new-v.old,values.duration,values.amplitude,values.period)
      end
    end
    return result
  end
  
  function instance:reset()
    values.time = 0
  end
  
  function instance:autoRepeats()
    return values.autoRepeat
  end
  
  function instance:deletesAfter()
    return values.deleteAfter
  end
  
  return instance
end

return animation