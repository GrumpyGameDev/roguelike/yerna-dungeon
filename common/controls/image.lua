local control = require "common.controls.control"
local imageControl = {}

function imageControl.new(parent,image,x,y,width,height)
  assert(type(parent)=="table" and getmetatable(parent)=="control","imageControl.new - parent must be a control")
  assert(type(image)=="userdata","imageControl.new - image must be a userdata")
  assert(type(x)=="number","imageControl.new - x must be a number")
  assert(type(y)=="number","imageControl.new - y must be a number")
  assert(type(width)=="number","imageControl.new - width must be a number")
  assert(type(height)=="number","imageControl.new - height must be a number")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  local values = {
    image = image
  }
  function instance:onDraw()
    love.graphics.draw(values.image,self:getX(),self:getY())
  end
  function instance:setImage(image)
    values.image = image
  end
  return instance
end


return imageControl