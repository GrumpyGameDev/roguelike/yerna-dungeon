local control = require "common.controls.control"
local stage = {}

function stage.new()
  local instance = control.new(nil)
  return instance
end

return stage