local control = require "common.controls.control"
local label = {}

function label.new(parent,font,text,align,x,y,width,height,color,hoverColor)
  local instance = control.new(parent)
  local values = {
    font = font,
    text = text,
    align = align
  }
  
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  instance:setLocalRed(color[1])
  instance:setLocalGreen(color[2])
  instance:setLocalBlue(color[3])
  instance:setLocalAlpha(color[4])
  instance:setLocal("hr",hoverColor[1])
  instance:setLocal("hg",hoverColor[2])
  instance:setLocal("hb",hoverColor[3])
  instance:setLocal("ha",hoverColor[4])

  function instance:onDraw()
    love.graphics.setColor(0,0,0,0)--this is very stupid, but makes the font not disappear
    love.graphics.rectangle("line",self:getX(),self:getY(),self:getWidth(),self:getHeight())--this is very stupid, but makes the font not disappear
    if self:isHover() then
      love.graphics.setColor(self:getLocal("hr"),self:getLocal("hg"),self:getLocal("hb"),self:getLocal("ha"))
    else
      love.graphics.setColor(self:getRed(),self:getGreen(),self:getBlue(),self:getAlpha())
    end
    love.graphics.setFont(values.font)
    love.graphics.printf(values.text,self:getX(),self:getY(),self:getWidth(),values.align)
  end
  
  function instance:setText(text)
    values.text=text
  end

  return instance
end


return label