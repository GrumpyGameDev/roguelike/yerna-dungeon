local control = require "common.controls.control"
local images = require "game.images"
local imageControl = require "common.controls.image"
local inventoryControl = {}

function inventoryControl.new(parent,x,y,width,height,columns,rows)
  assert(type(parent)=="table" and getmetatable(parent)=="control","inventoryControl.new - parent must be a control")
  assert(type(x)=="number","inventoryControl.new - x must be a number")
  assert(type(y)=="number","inventoryControl.new - y must be a number")
  assert(type(width)=="number","inventoryControl.new - width must be a number")
  assert(type(height)=="number","inventoryControl.new - height must be a number")
  assert(type(columns)=="number","inventoryControl.new - columns must be a number")
  assert(type(rows)=="number","inventoryControl.new - rows must be a number")
  local instance = control.new(parent)
  instance:setLocalX(x)
  instance:setLocalY(y)
  instance:setLocalWidth(width)
  instance:setLocalHeight(height)
  local values = {
    items={},
    itemImages={}
  }
  for row=1,rows do
    for column=1,columns do
      table.insert(values.itemImages,imageControl.new(instance,images.get("barbarian"),0,0,48,48))
      table.insert(values.items,"floor")
    end
  end
  
  function instance:preDraw()
    love.graphics.setScissor(self:getX(),self:getY(),self:getWidth(),self:getHeight())
  end
  
  function instance:postDraw()
    love.graphics.setScissor()
  end
  function instance:onDraw()
  end

  function instance:determineCurrentInventory()
    local current = {}
    for _,v in ipairs(values.items) do
      current[v] = (current[v] or 0) + 1
    end
    return current
  end
  
  function instance:determineDifferences(inventory)
    local current = self:determineCurrentInventory()
    local differences = {}
    for k,v in pairs(inventory) do
      differences[k] = (inventory[k] or 0) - (current[k] or 0)
    end
    return differences
  end
  
  function instance:findItemSlot(value)
    for i,v in ipairs(values.items) do
      if v==value then
        return i
      end
    end
    return nil
  end
  
  function instance:updateInventory(inventory)
    local differences = self:determineDifferences(inventory)
    for k,v in pairs(differences) do
      local slot
      while v<0 do
        slot = self:findItemSlot(k)
        values.itemImages[slot]:setImage(images.get("floor"))
        v=v+1
      end
      while v>0 do
        slot = self:findItemSlot("floor")
        values.itemImages[slot]:setImage(images.get(k))
        v=v-1
      end
    end
  end
  return instance
end


return inventoryControl