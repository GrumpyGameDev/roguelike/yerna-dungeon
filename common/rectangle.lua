local rectangle = {}

local mt = {
  __metatable="rectangle"
  }

function rectangle.new(x,y,w,h)
  x = x or 0
  y = y or 0
  w = w or 0
  h = h or 0
  
  local instance = {}
  setmetatable(instance,mt)
  
  local values = {x=0,y=0,w=0,h=0}
  
  function instance:getX()
    return values.x
  end
  
  function instance:getY()
    return values.y
  end
  
  function instance:getWidth()
    return values.w
  end
  
  function instance:getHeight()
    return values.h
  end
  
  function instance:setX(x)
    assert(type(x)=="number","rectangle:setX - attempted to set a non-numeric x")
    values.x=x
  end
  
  function instance:setY(y)
    assert(type(y)=="number","rectangle:setY - attempted to set a non-numeric y")
    values.y=y
  end
  
  function instance:setWidth(w)
    assert(type(w)=="number","rectangle:setWidth - attempted to set a non-numeric width")
    values.w=w
  end
  
  function instance:setHeight(h)
    assert(type(h)=="number","rectangle:setHeight - attempted to set a non-numeric height")
    values.h=h
  end
  
  function instance:contains(x,y)
    assert(type(x)=="number","rectangle:contains - x needs to be a number")
    assert(type(y)=="number","rectangle:contains - y needs to be a number")
    
    return x >= values.x and y >= values.y and x < values.x + values.w and y < values.y + values.h
  end
  
  function instance:copyFrom(other)
    assert(type(other)=="table" and getmetatable(other)=="rectangle","rectangle:copyFrom - attempted copying a non-rectangle")
    instance:setX(other:getX())
    instance:setY(other:getY())
    instance:setWidth(other:getWidth())
    instance:setHeight(other:getHeight())
  end
  
  instance:setX(x)
  instance:setY(y)
  instance:setWidth(w)
  instance:setHeight(h)
  
  return instance
end

function rectangle.copy(other)
  assert(type(other)=="table" and getmetatable(other)=="rectangle","rectangle.copy - attempted copying a non-rectangle")
  return rectangle.new(other:getX(),other:getY(),other.getWidth(),other.getHeight())
end

return rectangle