local preloads = require "preloads"
local bootstrapper = require "game.bootstrapper"
local stateMachine = require "game.states.machine"
--require("mobdebug").start()

print(tostring(love.filesystem))

function love.load()
  math.randomseed(os.time())  
  love.keyboard.setKeyRepeat(true)
  bootstrapper.load()
end

function love.update(dt)
  if stateMachine.update~=nil then
    stateMachine:update(dt)
  end
end

function love.draw()
  if stateMachine.draw~=nil then
    stateMachine:draw()
  end
end

function love.mousepressed(x,y,button,istouch)
  if stateMachine.mousepressed~=nil then
    stateMachine:mousepressed(x,y,button,istouch)
  end
end

function love.mousereleased(x,y,button,istouch)
  if stateMachine.mousereleased~=nil then
    stateMachine:mousereleased(x,y,button,istouch)
  end
end

function love.mousemoved(x,y,dx,dy,istouch)
  if stateMachine.mousemoved~=nil then
    stateMachine:mousemoved(x,y,dx,dy,istouch)
  end
end

function love.wheelmoved(x,y)
  if stateMachine.wheelmoved~=nil then
    stateMachine:wheelmoved(x,y)
  end
end

function love.filedropped(file)
  if stateMachine.filedropped~=nil then
    stateMachine:filedropped(file)
  end
end

function love.focus(focus)
  if stateMachine.focus~=nil then
    stateMachine:focus(focus)
  end
end

function love.visible(visible)
  if stateMachine.visible~=nil then
    stateMachine:visible(visible)
  end
end

function love.mousefocus(focus)
  if stateMachine.mousefocus~=nil then
    stateMachine:mousefocus(focus)
  end
end

function love.keypressed(key, scancode, isrepeat)
  if stateMachine.keypressed~=nil then
    stateMachine:keypressed(key, scancode, isrepeat)
  end
end

function love.keyreleased(key, scancode)
  if stateMachine.keyreleased~=nil then
    stateMachine:keyreleased(key, scancode)
  end
end

function love.resize(w, h)
  if stateMachine.resize~=nil then
    stateMachine:resize(w, h)
  end
end

function love.textedited(text, start, length)
  if stateMachine.textedited~=nil then
    stateMachine:textedited(text, start, length)
  end
end

function love.textinput(text)
  if stateMachine.textinput~=nil then
    stateMachine:textinput(text)
  end
end
