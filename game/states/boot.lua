local stage = require "game.stage"
local imageControl = require "common.controls.image"
local images = require "game.images"
return {
  update = function(self,dt)
    if self.background == nil then
      self.background = imageControl.new(stage,images.get("background"),0,0,1024,768)
    end
    self.stateMachine:setCurrent("title")
  end
  }
