local stage = require "game.stage"
local sceneControl = require "common.controls.scene"
local rectangleControl = require "common.controls.rectangle"
local animation = require "common.controls.animation"
local labelControl = require "common.controls.label"
local fonts = require "game.fonts"
local constants = require "game.constants"
return {
  controls={},
  transitioning=false,
  nextState = "boot",
  start = function(self)
    if self.root == nil then
      self.root = sceneControl.new(stage,0,0,constants.screen().width(),constants.screen().height())
      
      self.controls.start = labelControl.new(self.root,fonts.get("default"),"Start","right",128,0,128,44,{128,128,128,255},{128,128,128,255})
      self.controls.edit = labelControl.new(self.root,fonts.get("default"),"Edit","right",128,44,128,44,{255,0,0,255},{255,128,128,255})
      self.controls.about = labelControl.new(self.root,fonts.get("default"),"About","right",128,88,128,44,{255,0,0,255},{255,128,128,255})
      self.controls.options = labelControl.new(self.root,fonts.get("default"),"Options","right",128,132,128,44,{255,0,0,255},{255,128,128,255})
      self.controls.quit = labelControl.new(self.root,fonts.get("default"),"Quit","right",128,176,128,44,{128,128,128,255},{128,128,128,255})
    end
    self.root:enable()
    self.controls.start:addAnimation(animation.new("outElastic",1,{x={old=-128,new=768}}))
    self.controls.edit:addAnimation(animation.new("outElastic",2,{x={old=-128,new=768}}))
    self.controls.about:addAnimation(animation.new("outElastic",3,{x={old=-128,new=768}}))
    self.controls.options:addAnimation(animation.new("outElastic",4,{x={old=-128,new=768}}))
    self.controls.quit:addAnimation(animation.new("outElastic",5,{x={old=-128,new=768}}))
    self.transitioning=false
  end,
  draw = function(self)
    stage:draw()
  end,
  update = function(self, dt)
    stage:update(dt)
    if self.transitioning then
      if not stage:hasAnimation() then
        self.stateMachine:setCurrent(self.nextState)
        self.root:disable()
      end
    end
  end,
  mousereleased = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if self.controls.edit:isHover() then
        self.nextState = "editor"
        self:startTransition()
      elseif self.controls.start:isHover() then
        --self:startTransition()
      elseif self.controls.about:isHover() then
        self.nextState = "about"
        self:startTransition()
      elseif self.controls.options:isHover() then
        self.nextState = "options"
        self:startTransition()
      elseif self.controls.quit:isHover() then
        --self:startTransition()
      end
    end
  end,
  mousemoved = function(self, x, y, dx, dy, istouch)
    stage:checkHover(x,y)
  end,
  startTransition = function(self)
    self.transitioning=true
    for k,v in pairs(self.controls) do
      v:clearAnimations()
    end
    self.controls.start:addAnimation(animation.new("inBack",1,{x={old=768,new=-128}}))
    self.controls.edit:addAnimation(animation.new("inBack",1,{x={old=768,new=-128}}))
    self.controls.about:addAnimation(animation.new("inBack",1,{x={old=768,new=-128}}))
    self.controls.options:addAnimation(animation.new("inBack",1,{x={old=768,new=-128}}))
    self.controls.quit:addAnimation(animation.new("inBack",1,{x={old=768,new=-128}}))
  end
}
