local stage = require "game.stage"
local sceneControl = require "common.controls.scene"
local rectangleControl = require "common.controls.rectangle"
local animation = require "common.controls.animation"
local labelControl = require "common.controls.label"
local fonts = require "game.fonts"
local constants = require "game.constants"
local mapEditor = require "common.controls.mapeditor"
local level = require "game.level"
local buttonControl = require "common.controls.button"
local images = require "game.images"
local control = require "common.controls.control"
return {
  controls={},
  transitioning=false,
  nextState = "boot",
  start = function(self)
    if self.root == nil then
      self.root = sceneControl.new(stage,0,0,constants.screen().width(),constants.screen().height())
      self.controls.cancelButton = buttonControl.new(self.root,{normal=images.get("cancelButton"),hover=images.get("cancelButtonHover")},0,0,64,64)
    end
    self.root:enable()
    self.transitioning=false
  end,
  draw = function(self)
    stage:draw()
  end,
  update = function(self, dt)
    stage:update(dt)
    if self.transitioning then
      if not stage:hasAnimation() then
        self.stateMachine:setCurrent(self.nextState)
        self.root:disable()
      end
    end
  end,
  mousepressed = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
    end
  end,
  mousereleased = function(self, x, y, button, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
      if self.controls.cancelButton:isHover() then
        self.nextState = "title"
        self:startTransition()
      end
    end
  end,
  mousemoved = function(self, x, y, dx, dy, istouch)
    stage:checkHover(x,y)
    if not self.transitioning then
    end
  end,
  startTransition = function(self)
    self.transitioning=true
    for k,v in pairs(self.controls) do
      v:clearAnimations()
    end
  end
}
