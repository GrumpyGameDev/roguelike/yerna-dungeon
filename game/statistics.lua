local constants = require "game.constants"
local statistics = {}

local values = {
  
}
local resetvalues={
  {region= 1,oldX=192,oldY=192,newX=  0,newY=  0,oldAlpha=255,newAlpha=255},
  {region= 2,oldX=192,oldY=192,newX=128,newY=  0,oldAlpha=255,newAlpha=255},
  {region= 3,oldX=192,oldY=192,newX=256,newY=  0,oldAlpha=255,newAlpha=255},
  {region= 4,oldX=192,oldY=192,newX=384,newY=  0,oldAlpha=255,newAlpha=255},
  {region= 5,oldX=192,oldY=192,newX=  0,newY=128,oldAlpha=255,newAlpha=255},
  {region= 6,oldX=192,oldY=192,newX=128,newY=128,oldAlpha=255,newAlpha=255},
  {region= 7,oldX=192,oldY=192,newX=256,newY=128,oldAlpha=255,newAlpha=255},
  {region= 8,oldX=192,oldY=192,newX=384,newY=128,oldAlpha=255,newAlpha=255},
  {region= 9,oldX=192,oldY=192,newX=  0,newY=256,oldAlpha=255,newAlpha=255},
  {region=10,oldX=192,oldY=192,newX=128,newY=256,oldAlpha=255,newAlpha=255},
  {region=11,oldX=192,oldY=192,newX=256,newY=256,oldAlpha=255,newAlpha=255},
  {region=12,oldX=192,oldY=192,newX=384,newY=256,oldAlpha=255,newAlpha=255},
  {region=13,oldX=192,oldY=192,newX=  0,newY=384,oldAlpha=255,newAlpha=255},
  {region=14,oldX=192,oldY=192,newX=128,newY=384,oldAlpha=255,newAlpha=255},
  {region=15,oldX=192,oldY=192,newX=256,newY=384,oldAlpha=255,newAlpha=255},
  {region=16,oldX=192,oldY=192,newX=384,newY=384,oldAlpha=  0,newAlpha=  0},
}

function statistics.reset()
  values={}
  for i,v in ipairs(resetvalues) do
    local value = {}
    for k,vv in pairs(v) do
      value[k]=vv
    end
    values[i]=value
  end
end

function statistics.get(id)
  return values[id]
end

function statistics.swap(first,second)
  local temp = values[first]
  values[first]=values[second]
  values[second]=temp
  values[first].newX=values[second].oldX
  values[first].newY=values[second].oldY
  values[second].newX=values[first].oldX
  values[second].newY=values[first].oldY
end

local adjacentCells={
  {2,5},
  {1,3,6},
  {2,4,7},
  {3,8},
  {1,6,9},
  {2,5,7,10},
  {3,6,8,11},
  {4,7,12},
  {5,10,13},
  {6,9,11,14},
  {7,10,12,15},
  {8,11,16},
  {9,14},
  {10,13,15},
  {11,14,16},
  {12,15},
}

function isScrambled()
  for i,v in ipairs(values) do
    if i==v.region then
      return false
    end
  end
  return true
end

function findHole()
  for i,v in ipairs(values) do
    if v.region==16 then
      return i
    end
  end
end

function statistics.scramble()
  local count = 1000
  while count>0 or not isScrambled() do
    local hole = findHole()
    local adjacent = adjacentCells[hole][math.random(1,#adjacentCells[hole])]
    values[hole].region = values[adjacent].region
    values[hole].oldAlpha=255
    values[hole].newAlpha=255
    values[adjacent].region = 16
    values[adjacent].oldAlpha=0
    values[adjacent].newAlpha=0
    count=count-1
  end
end

function statistics.isSolved()
  for i,v in ipairs(values) do
    if i~=v.region then
      return false
    end
  end
  return true
end

return statistics