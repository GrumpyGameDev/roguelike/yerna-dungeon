local json = require "lunajson"
local level = {}

local mt = {__metatable="level"}

function level.new(columns,rows,boilerplate)
  assert(type(columns)=="number","level.new - columns must be a number")
  assert(type(rows)=="number","level.new - rows must be a number")
  assert(type(boilerplate)=="nil" or type(boilerplate)=="boolean","level.new - boilerplate must be nil or a boolean")
  boilerplate=boilerplate or false
  local instance = {}
  setmetatable(instance,mt)
  
  local values = {
    name="untitled",
    columns=columns,
    rows=rows,
    cells={},
    notes={},
    exits={}
  }
  
  for column=1,columns do
    table.insert(values.cells,{})
    for row=1,rows do
      local value = "floor"
      if boilerplate then
        if column==1 or column==columns or row==1 or row==rows then
          value="wall"
        elseif column==2 and row==2 then
          value="barbarian"
        elseif column==columns-1 and row==rows-1 then
          value="exit"
        end
      end
      table.insert(values.cells[column],value)
    end
  end
  
  function instance:getColumns()
    return values.columns
  end
  
  function instance:getRows()
    return values.rows
  end
  
  function instance:save(fileName)
    love.filesystem.write(fileName,json.encode(values))
  end
  
  function instance:getCell(column,row)
    assert(type(column)=="number","level:getCell - column must be a number")
    assert(type(row)=="number","level:getCell - row must be a number")
    if column<1 or column>self:getColumns() or row<1 or row>self:getRows() then
      return nil
    else
      return values.cells[column][row]
    end
  end
  
  function instance:clearSign(column,row)
    values.notes = values.notes or {}
    local removeMe = {}
    for i, v in ipairs(values.notes) do
      if v.column==column and v.row==row then
        table.insert(removeMe,1,i)
      end
    end
    for i, v in ipairs(removeMe) do
      table.remove(values.notes,v)
    end
  end
  
  function instance:clearExit(column,row)
    values.exits = values.exits or {}
    local removeMe = {}
    for i, v in ipairs(values.exits) do
      if v.column==column and v.row==row then
        table.insert(removeMe,1,i)
      end
    end
    for i, v in ipairs(removeMe) do
      table.remove(values.exits,v)
    end
  end
  
  function instance:addSign(column,row,text)
    self:clearSign(column,row)
    table.insert(values.notes,{
        column=column,
        row=row,
        text=text
        })
  end

  function instance:addExit(column,row,levelName)
    self:clearExit(column,row)
    table.insert(values.exits,{
        column=column,
        row=row,
        levelName=levelName
        })
  end
  
  function instance:getSign(column,row)
    for i,v in ipairs(values.notes) do
      if v.column==column and v.row==row then
        return v.text
      end
    end
    return nil
  end
  
  
  function instance:setCell(column,row,value)
    assert(type(column)=="number","level:setCell - column must be a number")
    assert(type(row)=="number","level:setCell - row must be a number")
    assert(type(value)=="string","level:setCell - value must be a string")
    assert(column>=1 and column<=self:getColumns(),"level:setCell - column must be in range")
    assert(row>=1 or row<=self:getRows(),"level:setCell - row must be in range")
    if values.cells[column][row]=="sign" then
      self:clearSign(column,row)
    elseif values.cells[column][row]=="exit" then
      self:clearExit(column,row)
    end
    values.cells[column][row]=value
    if value=="sign" then
      self:addSign(column,row,"sign text")
    elseif value=="exit" then
      self:addExit(column,row,"level name")
    end
  end
  
  function instance:findPlayer()
    for column=1,self:getColumns() do
      for row=1,self:getRows() do
        if values.cells[column][row]=="barbarian" then
          return column,row
        end
      end
    end
    return nil
  end
  
  function instance:canMove(deltaX,deltaY,inventory)
    local x,y = self:findPlayer()
    if x==nil then
      return false
    else
      x = x + deltaX
      y = y + deltaY
      if x<1 or y<1 or x>self:getColumns() or y>self:getRows() then
        return false
      else
        if self:getCell(x,y)=="wall" then
          return false
        elseif self:getCell(x,y)=="redLock" then
          return (inventory.redKey or 0) > 0
        elseif self:getCell(x,y)=="yellowLock" then
          return (inventory.yellowKey or 0) > 0
        elseif self:getCell(x,y)=="blueLock" then
          return (inventory.blueKey or 0) > 0
        else
          return true
        end
      end
    end
  end
  
  function instance:makeMove(deltaX,deltaY,inventory)
    if self:canMove(deltaX,deltaY,inventory) then
      local x,y = self:findPlayer()
      local nextX = x + deltaX
      local nextY = y + deltaY
      self:setCell(x,y,"floor")
      local result = self:getCell(nextX,nextY)
      self:setCell(nextX,nextY,"barbarian")
      if result=="redKey" or result=="yellowKey" or result=="blueKey" then
        inventory[result] = (inventory[result] or 0) + 1
      elseif result=="redLock" then
        inventory.redKey = (inventory.redKey or 0) - 1
      elseif result=="yellowLock" then
        inventory.yellowKey = (inventory.yellowKey or 0) - 1
      elseif result=="blueLock" then
        inventory.blueKey = (inventory.blueKey or 0) - 1
      end
      return inventory,result
    else
      return inventory
    end
  end
  
  function instance:getSignText(deltaX,deltaY)
    local x,y = self:findPlayer()
    local nextX = x + deltaX
    local nextY = y + deltaY
    for i,v in ipairs(values.notes) do
      if v.column==nextX and v.row==nextY then
        return v.text
      end
    end
    return nil
  end
  
  function instance:getNextLevel(deltaX,deltaY)
    local x,y = self:findPlayer()
    local nextX = x + deltaX
    local nextY = y + deltaY
    return nil
  end
  
  
  return instance
end

function level.load(fileName)
  --TODO: put this in a pcall to avoid exceptions
  assert(type(fileName)=="string","level.load - fileName must be a string")
  local data = love.filesystem.read(fileName)
  if data==nil then
    return nil
  end
  data = json.decode(data)
  if not type(data.columns)=="number" or not type(data.rows)=="number" or not type(data.cells)=="table" or not type(data.notes)=="table" then
    return nil
  end
  local instance=level.new(data.columns,data.rows)
  for column=1,data.columns do
    for row=1,data.rows do
      instance:setCell(column,row,data.cells[column][row])
    end
  end
  for i,v in ipairs(data.notes) do
    instance:addSign(v.column,v.row,v.text)
  end
  return instance
end

function level.copy(other)
  assert(type(other)=="table" and getmetatable(other)=="level","level.copy - other must be a level")
  local instance = level.new(other:getColumns(),other:getRows())
  for column=1,other:getColumns() do
    for row=1,other:getRows() do
      instance:setCell(column,row,other:getCell(column,row))
      local signText = other:getSign(column,row)
      if signText~=nil then
        instance:addSign(column,row,signText)
      end
    end
  end
  --TODO: exits
  return instance
end


return level