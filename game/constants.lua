local constants = {}

function constants.screen()
  return 
  {
    width=function () return 1024 end,
    height=function () return 768 end
  }
end

function constants.cell()
  return
  {
    width=function() return 48 end,
    height=function() return 48 end,
    columns=function() return 17 end,
    rows=function() return 17 end,
  }
end

function constants.mapView()
  return
  {
    width=function() return constants.cell().width() * (constants.cell().columns()-1) end,
    height=function() return constants.cell().height() * (constants.cell().rows()-1) end,
    x=function() return 0 end,
    y=function() return 0 end,
  }
  
end

return constants