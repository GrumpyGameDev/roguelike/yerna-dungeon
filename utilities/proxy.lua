local proxy = {}

function proxy.newProxy(object,name)
  assert(type(object)=="table","proxy.newProxy - object must be a table")
  assert(type(name)=="string" or type(name)=="nil","proxy.newProxy - name must be nil or a string")
  name = name or "generic proxy"
  local mt = {
    __index=function (t,k) 
      return object[k]
    end,
    __newindex=function(t,k,v) end,
    __metatable=name
  }

  local instance = {}
  setmetatable(instance,mt)
  return instance
end

return proxy.newProxy(proxy,"proxy")